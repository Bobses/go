# go

- https://gobyexample.com/
- https://play.golang.org/
- https://tour.golang.org/welcome/1


6 hours course: https://www.youtube.com/watch?v=YS4e4q9oBaU
 
Install GO on Fedora: https://developer.fedoraproject.org/tech/languages/go/go-installation.html


Commands to create a go build:

- <code>go run file_name.go</code>--> simply run a GO program
- <code>go build file_name.go</code> --> build the program and generate a compiled binary
- <code>./file_name</code> --> execute the previously created binary

