package main

import "fmt"

func main() {
	const _1mile = 1.609344
	var n, prod float64
	fmt.Print("Enter the number of miles: ")
	fmt.Scan(&n)
	prod = n * _1mile
	fmt.Println("The transformation in kilometers is: ", prod)
}
